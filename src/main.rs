use std::collections::{HashMap, HashSet};

fn main() {
    run(&parse());
}

fn run(cfg: &Config) {
    let bin = &cfg.bin;
    let files = &cfg.files;
    let mut symbols = get_entries(bin);
    let unresolved: HashSet<Entry> = symbols
        .clone()
        .into_iter()
        .filter(|e| e.s_type == "U")
        .collect();

    for file in files {
        symbols.extend(get_entries(file).into_iter());
    }

    resolve(&unresolved, &symbols);
}

fn resolve(unresolved: &HashSet<Entry>, symbols: &HashSet<Entry>) {
    let mut files = HashSet::new();
    for u_sym in unresolved {
        files.extend(
            symbols
                .iter()
                .filter(|e| e.s_type == "T" && e.name == u_sym.name)
                .map(|e| e.which),
        );
    }
    println!("{:?}", files);
}

fn get_entries(path: &str) -> HashSet<Entry> {
    use std::process::Command;
    let output = Command::new("nm").arg("-P").arg(path).output().unwrap();
    let output = std::str::from_utf8(&output.stdout).unwrap().to_owned();
    let lines: Vec<&str> = output.lines().filter(|line| !line.contains(":")).collect();
    let mut entries = HashSet::new();
    for line in lines {
        let mut elements = line.split(" ");
        let name = elements.next().unwrap().to_owned();
        let s_type = elements.next().unwrap().to_owned();
        let value = match elements.next() {
            None => None,
            Some(s) => {
                if s.is_empty() {
                    None
                } else {
                    Some(s.to_owned())
                }
            }
        };
        let size = match elements.next() {
            None => None,
            Some(s) => {
                if s.is_empty() {
                    None
                } else {
                    Some(s.to_owned())
                }
            }
        };
        let which = path;
        entries.insert(Entry {
            name,
            s_type,
            value,
            size,
            which,
        });
    }
    entries
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Entry<'a> {
    name: String,
    s_type: String,
    value: Option<String>,
    size: Option<String>,
    which: &'a str,
}

#[derive(Debug)]
struct Config {
    bin: String,
    files: Vec<String>,
}

fn parse() -> Config {
    use clap::*;
    let matches = App::new("Dep2Dot")
        .about("A tool to analyze symbol dependencies using `nm`")
        .arg(Arg::with_name("bin").required(true))
        .arg(Arg::with_name("files").required(true).multiple(true))
        .get_matches();
    let bin = matches.value_of("bin").unwrap().to_owned();
    let files: Vec<String> = matches
        .values_of("files")
        .unwrap()
        .map(|s| s.to_owned())
        .collect();
    Config { bin, files }
}
